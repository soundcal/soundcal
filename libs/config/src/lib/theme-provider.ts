import { createTheme, responsiveFontSizes } from '@mui/material/styles';
import { themeOptions } from './theme';
const initTheme = createTheme(themeOptions);
export const theme = responsiveFontSizes(initTheme);

// export { theme };
