import { ThemeProvider as EmotionTheme } from '@emotion/react';
import { CssBaseline } from '@mui/material';
import { ThemeProvider } from '@mui/material/styles';
import { theme } from '@soundcal/config';
import React from 'react';
// console.log(theme);
// export const decorators = [
//   (Story) => (
//     <>
//       <CssBaseline />
//       <ThemeProvider theme={theme}>
//         <Story />
//       </ThemeProvider>
//     </>
//   ),
// ];

// addDecorator((Story) => {
//   console.log('in decorator');
//   return (
//     <>
//       <CssBaseline />
//       <ThemeProvider theme={theme}>
//         <Story />
//       </ThemeProvider>
//     </>
//   );
// });

export const decorators = [
  (Story) => {
    console.log('in decorator');
    return (
      <EmotionTheme theme={theme}>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          {/* <React.StrictMode> */}
          <Story />
          {/* </React.StrictMode> */}
        </ThemeProvider>
      </EmotionTheme>
    );
  },
];
