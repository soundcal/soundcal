import { Typography } from '@mui/material';
import styles from './ui.module.css';

/* eslint-disable-next-line */
export interface UiProps {}

export function Ui(props: UiProps) {
  return (
    <div className={styles['container']}>
      <Typography>Welcome to Ui!</Typography>
    </div>
  );
}

export default Ui;
