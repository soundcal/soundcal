import { Button, Grid } from '@mui/material';
import { Meta, Story } from '@storybook/react';
import { RootLayout, RootLayoutProps } from './root-layout';

export default {
  component: RootLayout,
  title: 'RootLayout',
} as Meta;

const Template: Story<RootLayoutProps> = (args) => <RootLayout {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  children: (
    <Grid item>
      <Button variant="contained">Sample</Button>
    </Grid>
  ),
};
