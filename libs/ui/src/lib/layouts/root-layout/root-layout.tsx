import { Container, Grid } from '@mui/material';

/* eslint-disable-next-line */
export interface RootLayoutProps {
  children: React.ReactNode | React.ReactNode[];
}

export function RootLayout(props: RootLayoutProps) {
  const { children } = props;
  return (
    <Container>
      <Grid container spacing={3}>
        {children}
      </Grid>
    </Container>
  );
}

export default RootLayout;
