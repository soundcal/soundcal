import { Box, Button, Grid, Paper, Typography } from '@mui/material';

/* eslint-disable-next-line */
export interface HeroProps {
  header?: string;
  subheader?: string;
  image?: string;
}

export function Hero(props: HeroProps) {
  const { header, subheader, image } = props;
  return (
    <Box
      sx={{
        // backgroundColor: (theme) => theme.palette.background.paper,

        width: '100%',
        display: 'flex',
        // minHeight: '600px',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundImage: `url("${
          image ||
          'https://images.unsplash.com/photo-1493225457124-a3eb161ffa5f?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=80&raw_url=true&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1000'
        }")`,
        backgroundPosition: 'center',
        borderRadius: (theme) => theme.shape.borderRadius,
        boxShadow: (theme) => theme.shadows[5],
        overflow: 'clip',
      }}
    >
      <Paper
        sx={{
          // height: '100%',
          background:
            'linear-gradient(to top, rgba(0, 0, 0, 0.85), transparent)',
          boxShadow: 'none',
        }}
      >
        <Grid
          container
          spacing={6}
          sx={{
            color: (theme) => theme.palette.getContrastText('#1d1e20'),
            display: 'flex',
            alignItems: 'center',
            maxWidth: '1300px',
            padding: '50px',
          }}
        >
          <Grid
            item
            xs={12}
            md={7}
            sx={{
              fontAlign: 'center',
            }}
          >
            <Typography variant="h3" fontWeight={700}>
              {header || "Let's scale your business"}
            </Typography>
            <Typography variant="h6">
              {subheader ||
                `Hire professionals who will help your business make 10X your
            previous income. With over 5years experience in Marketing & Business
            strategy, we are your best client.`}
            </Typography>
            <Button
              variant="contained"
              color="primary"
              sx={{ width: '200px', fontSize: '16px' }}
            >
              HIRE US
            </Button>
          </Grid>
          {/* <Grid item xs={12} md={5}>
            <img
              src={`https://images.unsplash.com/photo-1493225457124-a3eb161ffa5f?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=80&raw_url=true&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1000`}
              alt="My Team"
              style={{
                width: '100%',
              }}
            />
          </Grid> */}
        </Grid>
      </Paper>
    </Box>
  );
}

export default Hero;
