import { Meta, Story } from '@storybook/react';
import { Hero, HeroProps } from './hero';
export default {
  component: Hero,
  title: 'Hero',
} as Meta;

const Template: Story<HeroProps> = (args) => (
  // <ThemeProvider theme={theme}>
  <Hero {...args} />
  // </ThemeProvider>
);

export const Primary = Template.bind({});
Primary.args = {
  header: 'Soundcal',
};
