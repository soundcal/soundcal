import { Hero } from '@soundcal/ui';

/* eslint-disable-next-line */
export interface HomePageProps {}

export function HomePage(props: HomePageProps) {
  return (
    <Hero header="Sample" />
    // <div className={styles['container']}>
    //   <Typography variant="h1">Welcome to HomePage!</Typography>
    // </div>
  );
}

export default HomePage;
