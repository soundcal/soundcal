import { createTheme, responsiveFontSizes } from '@mui/material/styles';
import { themeOptions } from '@soundcal/config';
let theme = createTheme(themeOptions);
theme = responsiveFontSizes(theme);

export default theme;
